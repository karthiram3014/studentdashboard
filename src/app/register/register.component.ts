import { Component,OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {



  studentDetails:any;
  viewStudent:any;
  studentToView={
    id:"",
    name:"",
    email:"",
    address:"",
    fname:"",
    tname:"",
    standard:"",
    percentage:"",
    phone:""
  };
 
  studentToUpdate={
    id:"",
    name:"",
    email:"",
    address:"",
    fname:"",
    tname:"",
    standard:"",
    percentage:"",
    phone:""
  };
  //popup:boolean=false;
  constructor(private studentService:StudentService,private router :Router){
   
  }
  ngOnInit(){
    this.getStudentDetails();
  }
  register(registerForm:NgForm){

    this.studentService.registerStudent(registerForm.value).subscribe(
      (resp)=>{
        console.log(resp);
        registerForm.reset();
        this.router.navigate(['/home']);
         this.getStudentDetails();
      },
      (err)=>{
        console.log(err);
      })
    ;
   

  }
getStudentDetails(){
  this.studentService.getStudents().subscribe(
    (resp)=>{
      console.log(resp);
      this.studentDetails=resp;
    },
    (err)=>{
      console.log(err);
    }
  );
}

getStudentDetailsById(id:any){

  this.studentService.getStudentsById(id).subscribe(
    (resp)=>{
      console.log(resp);
      console.log(this.studentDetails)
      this.viewStudent=resp;
      console.log(this.studentDetails)
    },
    (err)=>{
      console.log(err);
    }
  );
}

deleteStudent(student:any){
  console.log(student.id)
  this.studentService.deleteStudent(student.id).subscribe(
    (resp)=>{
      console.log(resp);
      
     
      this.getStudentDetails();
      

    },
    (err)=>{
      console.log(err);
      this.getStudentDetails();
    } 
  );
}


edit(student:any){
  this.studentToUpdate=student;
 // this.popup=true;
}
updateStudent(){
  console.log( this.studentToUpdate);
  this.studentService.updateStudent(this.studentToUpdate).subscribe(
    (resp: any)=>{
      console.log(resp);
      //this.popup=false;
    },
    (err: any)=>{
      console.log(err);
    }
  );
}
view(student:any){
  // this.studentToView=student;
  console.log(student);
  this.getStudentDetailsById(student.id);
}



  id:String='';
  name:String='';
  email:String='';
  adddress:String='';
  fname:String='';
  tname:String='';
 
  standard:Number=0;
  percentage:Number=0;
  phone:String='';
 
}
