    import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class StudentService {
  [x: string]: any;

  constructor(private http:HttpClient) { }

  API='http://localhost:8080';

  public registerStudent(studentData:any){
    return this.http.post(this.API +'/create',studentData);
  }

  public getStudents(){
    return this.http.get(this.API +'/get');
  }
  public getStudentsById(id:any){
    return this.http.get(this.API +'/get'+'/'+id);
  }




  public deleteStudent(id: any){
    console.log(this.API +`/${id}`)

    return this.http.delete(this.API +`/${id}`);
    
  }

  public updateStudent(student:any){
    return this.http.put(this.API +'/update',student);
  
  }     }



  /*export class AuthService {
    private baseUrl = 'http://localhost:8080'; 
    constructor(private http: HttpClient) { }
  
    checkUser(userId: string, password: string): Observable<boolean> {
      const url = `${this.baseUrl}/checkUser`;
      const body = { userId, password };
      return this.http.post<boolean>(url, body);
    }
  }*/

  

 /* public checkUserId(userId:any,password:any){
    return this.http.post(this.API+'/login'+`/${userId}`+`/${password}`);
  }
}*/
