import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { StudentComponent } from './student/student.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {path:'',component:LoginComponent},
  { path:'register',component:RegisterComponent},
 { path:'home',component:StudentComponent}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
